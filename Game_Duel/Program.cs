using System;

namespace JogoDueloSofis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            Guerreiro g1 = new Guerreiro(string.Empty); 
            Guerreiro g2 = new Guerreiro(string.Empty);
            Mago m1 = new Mago(string.Empty); 
            Mago m2 = new Mago(string.Empty);

            string tipo1 = "";
            string tipo2 = "";

            Console.WriteLine("Player 1: Choice your class:");
            Console.WriteLine("1 - Warrior");
            Console.WriteLine("2 - Wizard");
            Console.WriteLine("Type your option? (1 or 2)");
            int opcao = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Type your character's name: ");
            string nomePersonagem = Console.ReadLine();

            switch (opcao)
            {
                case 1:
                    g1 = new Guerreiro(nomePersonagem);
                    break;
                case 2:
                    m1 = new Mago(nomePersonagem);
                    break;
                default:
                    break;
            }

            Console.WriteLine("Player 2: Choice your class:");
            Console.WriteLine("1 - Warrior");
            Console.WriteLine("2 - Wizard");
            Console.WriteLine("Choice your option? (1 or 2)");
            opcao = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Type your character's name: ");
            nomePersonagem = Console.ReadLine();

            switch (opcao)
            {
                case 1:
                    g2 = new Guerreiro(nomePersonagem);
                    break;
                case 2:
                    m2 = new Mago(nomePersonagem);
                    break;
                default:
                    break;
            }


            Console.WriteLine("Name\tLife\tAttack\tDefense\tClass");
            Console.WriteLine("");
            if(g1.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", g1.Nome, g1.PV, g1.Ataque, g1.Defesa, g1.Tipo);
            }
            else if(m1.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", m1.Nome, m1.PV, m1.Ataque, m1.Defesa, m1.Tipo);
            }
            if(g2.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", g2.Nome, g2.PV, g2.Ataque, g2.Defesa, g2.Tipo);
            }
            else if(m2.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", m2.Nome, m2.PV, m2.Ataque, m2.Defesa, m2.Tipo);
            }

            Console.WriteLine("");

            if(g1.Nome != string.Empty)
                {
                    Console.WriteLine("---------------------------------------");
                    Console.WriteLine("Choice a type of sword {0}:", g1.Nome);
                    Console.WriteLine("Short");
                    Console.WriteLine("Long");
                    Console.WriteLine("Type exactly one of the options: ");
                    tipo1 = Console.ReadLine();
                }
                
            else if(m1.Nome != string.Empty)
                {
                    Console.WriteLine("---------------------------------------");
                    Console.WriteLine("Choice a type of power {0}:", m1.Nome);
                    Console.WriteLine("Fire");
                    Console.WriteLine("Ice");
                    Console.WriteLine("Lightning");
                    Console.WriteLine("Type exactly one of the options: ");
                    tipo1 = Console.ReadLine();
                }  
            if(g2.Nome != string.Empty)
                {
                    Console.WriteLine("---------------------------------------");
                    Console.WriteLine("Choice a type of sword {0}:", g2.Nome);
                    Console.WriteLine("Short");
                    Console.WriteLine("Long");
                    Console.WriteLine("Type exactly one of the options: ");
                    tipo2 = Console.ReadLine();
                }
            else if(m2.Nome != string.Empty)
                {
                   Console.WriteLine("---------------------------------------");
                    Console.WriteLine("Choice a type of power {0}:", m2.Nome);
                    Console.WriteLine("Fire");
                    Console.WriteLine("Ice");
                    Console.WriteLine("Lightning");
                    Console.WriteLine("Type exactly one of the options: ");
                    tipo2 = Console.ReadLine();
                }
            if(g1.Nome != string.Empty)
            {
                g1.AtacarComEspada(tipo1);
            }
            else if(m1.Nome != string.Empty)
            {
                 m1.AtacarComMagia(tipo1);
            }
            if(g2.Nome != string.Empty)
            {
                g2.AtacarComEspada(tipo2);
            }
            else if(m2.Nome != string.Empty)
            {
                 m2.AtacarComMagia(tipo2);
            }

            Console.Clear();

            if(g1.Nome != string.Empty && g2.Nome != string.Empty)
            {
                g1.Defender(g2.Ataque);
            }
            else
            {
                g1.Defender(m2.Ataque);
            }

            if(m1.Nome != string.Empty && m2.Nome != string.Empty)
            {
                m1.Defender(m2.Ataque);
            }
            else
            {
                m1.Defender(g2.Ataque);
            }

            if(g1.Nome == string.Empty && g2.Nome != string.Empty)
            {
                g2.Defender(m1.Ataque);
            }
            else
            {
                g2.Defender(g1.Ataque);
            }

            if(m2.Nome != string.Empty && m1.Nome != string.Empty)
            {
                m2.Defender(m1.Ataque);
            }
            else
            {
                m2.Defender(g1.Ataque);
            }


            Console.WriteLine("Name\tLife");
            if(g1.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}", g1.Nome, g1.PV);
            }
            else
            {
                Console.WriteLine("{0}\t{1}", m1.Nome, m1.PV);
            }
            if(g2.Nome != string.Empty)
            {
                Console.WriteLine("{0}\t{1}", g2.Nome, g2.PV);
            }
            else
            {
                Console.WriteLine("{0}\t{1}", m2.Nome, m2.PV);
            }

            Console.WriteLine("");

            if(g1.PV > g2.PV || g1.PV > m2.PV)
            {
                Console.WriteLine("{0} won the duel!", g1.Nome);
            }
            else if(m1.PV > g2.PV || m1.PV > m2.PV)
            {
                Console.WriteLine("{0} won the duel!", m1.Nome);
            }
            
            if(g2.PV > g1.PV || g2.PV > m2.PV)
            {
                Console.WriteLine("{0} won the duel!", g2.Nome);
            }
            else if(m2.PV > g1.PV || m2.PV > m1.PV)
            {
                Console.WriteLine("{0} won the duel!", m2.Nome);
            }
                
        }
    }
}
