namespace JogoDueloSofis
{
    public class Guerreiro : Personagem
    {
        public Guerreiro (string nome)
        {
            this.Nome = nome;
            this.Ataque = 30;
            this.Defesa = 20;
            this.PV = 10;
            this.Tipo = "Warrior";
        }
        public int Atacar()
        {
            return this.Ataque;
        }
        public int AtacarComEspada(string tipo)
        {
            if(tipo == "Short")
            {
                this.Ataque += 3;
            }
            else if(tipo == "Long")
            {
                this.Ataque += 4;
            }
            return this.Ataque;
        }
        public void Defender(int ataqueInimigo)
        {
            bool danoRecebido = this.Defesa < ataqueInimigo;
            if(danoRecebido)
            {
                int dano = ataqueInimigo - this.Defesa;
                this.PV -= dano;
            }
            else
            {
                this.PV = this.PV;
            }
        }
    }
}
