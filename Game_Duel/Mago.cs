namespace JogoDueloSofis
{
    public class Mago : Personagem
    {
        public Mago (string nome)
        {
            this.Nome = nome;
            this.Ataque = 20;
            this.Defesa = 10;
            this.PV = 30;
            this.Tipo = "Mago";
        }
        public int Atacar()
        {
            return this.Ataque += 1;
        }
        public int AtacarComMagia(string tipo)
        {
            if(tipo == "Fogo")
            {
                this.Ataque += 10;
            }
            else if(tipo == "Gelo")
            {
                this.Ataque += 15;
            }
            else if(tipo == "Raio")
            {
                this.Ataque += 25;
            }
            return this.Ataque;
        }
        public void Defender(int ataqueInimigo)
        {
            bool danoRecebido = this.Defesa < ataqueInimigo;
            if(danoRecebido)
            {
                int dano = ataqueInimigo - this.Defesa;
                this.PV -= dano;
            }
            else
            {
                this.PV = this.PV;
            }
        }
    }
}
